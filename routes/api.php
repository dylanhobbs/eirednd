<?php

use Illuminate\Http\Request;
use App\Http\Resources\User as UserResource;
use \Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*
 * Game Routes
 */
// List all games
Route::get('games', 'GameController@index');

// List all game markers
Route::get('markers', 'GameController@markers');

// Get single Game
Route::get('games/{id}', 'GameController@show');

// Create new Game
Route::post('games', 'GameController@store');

// Update a game
Route::put('games/{id}', 'GameController@show');

// Delete a game
Route::delete('games/{id}', 'GameController@destroy');


/*
 * User Routes
 */
//Route::middleware('auth:api')
Route::group(['middleware' => 'web'], function () {
    Route::get('me', function(Request $request){
       return new UserResource($request->user());
    });
});



<?php

namespace App;

use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    use SpatialTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'time', 'blurb',
    ];

    protected $spatialFields = [
        'location',
    ];

    /**
     * The sessions the user has rsvpd to
     */
    public function sessions(){
        return $this->belongsToMany('App\User', 'rsvp');
    }

}

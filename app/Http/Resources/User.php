<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'username' => $this->username,
            'email' => $this->email,
            'member_since' => $this->created_at->diffInDays(Carbon::now()),
            'player_rating' => $this->player_rating === 0 ? 'NA' : $this->player_rating ,
            'dm_rating' => $this->dm_rating === 0 ?  'NA' : $this->dm_rating,
            'avatar' => $this->avatar,
            'games_in' => $this->games->count(),
            'games' => Game::collection($this->games)

        ];
    }
}

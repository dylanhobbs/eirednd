<?php

namespace App\Http\Resources;

use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Http\Resources\Json\JsonResource;

class GameMarker extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'Feature',
            'properties' => [
                'id' => $this->id,
                'name' => $this->name,
                'rpg' => $this->rpg->name,
                'current_players' => $this->current_players,
                'max_players' => $this->max_players,
            ],
            'geometry' => [
                'type' => 'Point',
                'coordinates' => array($this->location->getLat(), $this->location->getLng()),
            ],
        ];
    }
}

<?php

namespace App\Http\Controllers;


use App\Game;
use App\Http\Resources\Game as GameResource;
use App\Http\Resources\GameMarker;
use App\RPG;
use Grimzy\LaravelMysqlSpatial\Types\LineString;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Grimzy\LaravelMysqlSpatial\Types\Polygon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        $games = Game::with('rpg')->paginate(100);
        return GameResource::collection($games);
    }

    /**
     * Display a listing of the resource using a marker.
     *
     * @return AnonymousResourceCollection
     */
    public function markers(Request $request)
    {
        $request->validate([
            'x1' => 'required',
            'y1' => 'required',
            'x2' => 'required',
            'y2' => 'required',
        ]);
        $x1 = $request->x1;
        $y1 = $request->y1;
        $x2 = $request->x2;
        $y2 = $request->y2;
        $polygon = new Polygon([new LineString([
            new Point($y1, $x1),
            new Point($y2, $x1),
            new Point($y2, $x2),
            new Point($y1, $x2),
            new Point($y1, $x1)
        ])]);

        $games = \App\Game::within('location', $polygon)->with('rpg')->paginate(150);
        return GameMarker::collection($games);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'game' => 'required',
            'name' => 'required',
            'description' => 'required|max:3000',
            'welcoming_level' => 'required',
            'max_players' => 'required',
            'starting_level' => 'required',
            'location' => 'required',
        ]);
        $game = $request->game;
        $name = $request->name;
        $description = $request->description;
        $welcoming_level = $request->welcoming_level;
        $max_players = $request->max_players;
        $starting_level = $request->starting_level;
        $location = $request->location;

        $location = new \Grimzy\LaravelMysqlSpatial\Types\Point($location->lat, $location->long);


        $frequency = $request->frequency ?: null;
        $session_length = $request->session_length ?: null;
        $min_age = $request->min_age ?: null;
        $expected_sessions = $request->expected_sessions ?: null;
        $online_only = $request->online_only ?: null;
        $roll20 = $request->roll20 ?: null;

        $game_data = [
            'name' => $name,
            'description' => $description,
            'picture' => '',
            'max_players' => $max_players,
            'expected_sessions' => $expected_sessions,
            'frequency' => $frequency,
            'session_time' => $session_length,
            'schedule' => '',
            'starting_level' => $starting_level,
            'welcoming_level' => $welcoming_level,
            'style' => 'Stylish',
            'minimum_age' => $min_age,
            'online' => $online_only,
            'roll_20_url' => $roll20,
            'location' => $location,
        ];

        $rpg = RPG::where('name', $game);

        $game = Game::create($game_data);
        $game->rpg()->attach($rpg->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RPG extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'release', 'blurb', 'link'
    ];


    /**
     * Get the games that use this RPG
     */
    public function games() {
        return $this->hasMany('App\Game', 'rpg_id');
    }
}

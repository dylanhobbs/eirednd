<?php

namespace App;

use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    use SpatialTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'picture', 'max_players', 'expected_sessions', 'frequency',
        'session_time', 'schedule', 'starting_level', 'welcoming_level', 'style',
        'minimum_age', 'online', 'roll_20_url'
    ];

    protected $spatialFields = [
        'location',
    ];

    /**
     * The players in this game
     */
    public function players()
    {
        return $this->belongsToMany('App\User', 'user_game');
    }

    /**
     * The sessions for this game
     */
    public function sessions()
    {
        return $this->hasMany('App\Session');
    }

    /**
     * Get the games chosen RPG
     */
    public function rpg()
    {
        return $this->belongsTo('App\RPG');
    }

}

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import Buefy from 'buefy'

window.Vue = require('vue');

/*
    Auto Load all the Vue stuff
*/
const files = require.context('./', true, /\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));


Vue.use(Buefy, {
    defaultIconPack: 'fas',
});

/*
    Vee-Validate Config
*/
import { extend } from 'vee-validate';
import { required } from 'vee-validate/dist/rules';
// Add the required rule
extend('required', {
    ...required,
    message: 'This field is required'
});
import { ValidationProvider } from 'vee-validate';
// Register it globally
// main.js or any entry file.
Vue.component('ValidationProvider', ValidationProvider);

/*
    Start the Vue Instance
*/
const app = new Vue({
    el: '#app',
});

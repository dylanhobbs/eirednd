// This is used for simple global state management
// If you come back here in the future to add more things consider Vuex
// Information: https://alligator.io/vuejs/global-event-bus/
import Vue from 'vue';
export const EventBus = new Vue();

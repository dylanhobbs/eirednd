@section('nav')
<nav class="navbar" role="navigation" aria-label="main navigation">
    <div class="navbar-brand">
        <a class="navbar-item" href="{{ url('/') }}">
            {{ config('app.name', 'EireDnD') }}
        </a>

        <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
        </a>
    </div>

    <div class="navbar-menu">
        @guest

        @else
        <div class="navbar-start">
            <div class="navbar-item">
                <div class="field is-grouped">
                    <search-bar></search-bar>
                </div>
            </div>
        </div>
        @endguest

        <div class="navbar-end">
            <div class="navbar-item">
                @guest
                    <div class="buttons">
                        @if (Route::has('register'))
                            <a class="button is-primary" href="{{ route('register') }}">{{ __('Register') }}</a>
                        @endif
                        <a class="button is-light" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </div>
                @else
                    @if(Route::currentRouteName() === 'home')
                    <div class="navbar-item">
                        <a href="{{ route('profile') }}" class="button is-primary is-rounded">
                            Your Games
                        </a>
                    </div>
                    @else
                    <div class="navbar-item">
                        <a href="{{ route('home') }}" class="button is-primary is-rounded">
                            Map View
                        </a>
                    </div>
                    @endif
                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <div class="navbar-dropdown">
                            <a class="navbar-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </div>
                @endguest
            </div>
        </div>
    </div>
</nav>
@endsection

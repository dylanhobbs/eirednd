<?php

use App\RPG;
use App\Session;
use App\Game;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 10)->create()->each(function (User $user) {
                $user->games()->saveMany(
                factory(Game::class, 4)->create()->each(function (Game $game){
                    $game->rpg()->associate(factory(RPG::class)->create());
                    $game->sessions()->saveMany(factory(Session::class, 15)->make());
                })
            );
        });
    }
}

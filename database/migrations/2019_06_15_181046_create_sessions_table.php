<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sessions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable()->default(null)->comment('This should be set to "Session {ID+1}"');
            $table->dateTime('time')->nullable()->default(null);
            $table->point('location')->nullable()->default(null);
            $table->text('blurb')->nullable()->default(null);
            $table->unsignedBigInteger('game_id')->nullable()->default(null);
            $table->foreign('game_id')->references('id')->on('games');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sessions');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->default('An RPG');
            $table->string('description')->default('Roll for initiative');
            $table->string('picture')->default('game.jpg');
            $table->integer('max_players');
            $table->integer('current_players');
            $table->integer('expected_sessions');
            $table->integer('rating')->default(0);
            $table->string('frequency')->default('weekly');
            $table->string('session_time')->default('evening');
            $table->string('schedule')->default('weekend / weekly');
            $table->integer('starting_level')->default(0);
            $table->string('welcoming_level')->nullable()->default(null);
            $table->string('style')->nullable()->default(null);
            $table->integer('minimum_age')->default(16);
            $table->boolean('online')->default(false);
            $table->string('roll_20_url')->nullable()->default(null);
            $table->point('location');
            $table->unsignedBigInteger('rpg_id')->nullable()->default(null);
            $table->foreign('rpg_id')->references('id')->on('r_p_g_s');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}

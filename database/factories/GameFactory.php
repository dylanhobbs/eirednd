<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Game;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Game::class, function (Faker $faker){
    $lat = -7.557562111111;
    $long = 53.473127111111;
    $offset = $faker->randomFloat(12, 0, 1.25);
    $offset2 = $faker->randomFloat(12, 0, 1.25);
    $rand = mt_rand(0,4);
    switch ($rand){
        case(4):
            $lat += $offset;
            $long += $offset2;
            break;
        case(3):
            $lat -= $offset;
            $long -= $offset2;
            break;
        case(2):
            $lat += $offset;
            $long -= $offset2;
            break;
        case(1):
            $lat -= $offset;
            $long += $offset2;
    }

    $max_players = $faker->numberBetween(0, 10);
    $current_players = $faker->numberBetween(0, $max_players);
    return [
        'name' => $faker->company,
        'max_players' => $max_players,
        'current_players' => $current_players,
        'expected_sessions' => $faker->randomDigitNotNull,
        'frequency' => $faker->randomElement(array('weekly', 'fortnightly', 'bi-weekly', 'daily')),
        'session_time' => $faker->randomElement(array('evening', 'afternoon', 'morning')),
        'starting_level' => $faker->numberBetween(0, 20),
        'welcoming_level' => $faker->randomElement(array('newbie', 'intermediate', 'advanced')),
        'style' => $faker->randomElement(array('role-play', 'combat', 'balanced', null)),
        'minimum_age' => $faker->numberBetween(16,25),
        'online' => $faker->boolean,
        'roll_20_url' => $faker->url,
        'location' => new \Grimzy\LaravelMysqlSpatial\Types\Point($lat, $long),
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Session;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Session::class, function (Faker $faker){
    $lat = -7.557562;
    $long = 53.473127;
    $offset = $faker->randomFloat(6, 0, 1);
    $offset2 = $faker->randomFloat(6, 0, 1);
    $lat += $offset;
    $long += $offset2;
    return [
        'name' => $faker->company,
        'time' => $faker->dateTimeBetween('now', '+1 month'),
        'blurb' => $faker->text(240),
        'location' => new \Grimzy\LaravelMysqlSpatial\Types\Point($lat, $long),
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\RPG;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(RPG::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'release' => $faker->dateTimeThisDecade(\Carbon\Carbon::now()),
        'blurb' => $faker->text(500),
        'link' => $faker->url
    ];
});
